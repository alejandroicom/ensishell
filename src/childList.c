#include "childList.h"

typedef struct child
{
	pid_t pid;
	char* name;
	struct child* next;
} child;

childList newList()
{
	child** result = malloc(sizeof(child*()));
	*result = NULL;
	return result;
}

void freeList(childList list) // As free in C++ it doesn't change the pointer value.
{
	Child *current = *list;
	Child *next = current;
	if (*list != NULL)
	{
		do
		{
			next=next->next;
			free(current);
			current=next;
		} while (current->next != NULL);
	}
}

void insertChild(pid_t pid, char* name, childList list)
{	
	Child* backup = *list;
	*list = malloc(sizeof(Child()));
	(*list)->pid = pid;
	(*list)->name = malloc(strlen(name) + 1);
	strcpy ((*list)->name, name);
	(*list)->next = backup;
}

void deleteChild(pid_t pid, childList list)
{
	Child *aux, *current;
	if (*list == NULL)
	{
		return;
	}
	if ((*list)->pid == pid)
	{
		aux = *list;
		*list = (*list)->next;
		free(aux);
	}
	else
	{
		current=(*list)->next;
		aux=*list;
		while (current!=NULL)
		{
			if (current->pid==pid)
			{
				aux->next=current->next;
				free(current);
				break;
			}
			current=current->next;
			aux=aux->next;
		}
	}
}

int listLength(childList list)
{
	int result = 0;
	if (!(list==NULL || *list==NULL))
	{
		Child *current = *list;
		while (current != NULL)
		{
			result++;
			current=current->next;
		}
	}
	return result;
}

char* getName(int i, childList list)
{
	Child *current = *list;
	while (i != 1)
	{
		i--;
		current=current->next;
	}
	return current->name;
}

pid_t getPID(int i, childList list)
{
	Child *current = *list;
	while (i != 1)
	{
		i--;
		current=current->next;
	}
	return current->pid;

}

void printList(childList list)
{
	Child *current = *list;
	while (current != NULL)
	{
		printf("%d: %s\n", current->pid, current->name);
		current=current->next;
	}
}