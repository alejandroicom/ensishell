#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct child Child;

typedef Child** childList;

childList newList();//On first time is needed to initialize with this and after any removeList

void insertChild(pid_t pid, char* name, childList list);

void deleteChild(pid_t pid, childList list);

int listLength(childList list);

char* getName(int i, childList list);

pid_t getPID(int i, childList list);

void printList(childList list);

void freeList(childList list);