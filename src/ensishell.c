/***********************************0******************
 * Copyright Grégory Mounié 2008-2015                *
 *           Simon Nieuviarts 2002-2009              *
 * This code is distributed under the GLPv3 licence. *
 * Ce code est distribué sous la licence GPLv3+.     *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
 
#include "childList.h"
#include "variante.h"
#include "readcmd.h"

#ifndef VARIANTE
#error "Variante non défini !!"
#endif

/* Guile (1.8 and 2.0) is auto-detected by cmake */
/* To disable Scheme interpreter (Guile support), comment the
 * following lines.  You may also have to comment related pkg-config
 * lines in CMakeLists.txt.
 */

#if USE_GUILE == 1
#include <libguile.h>

childList liste;

void terminate(char *line);
void executePipe(struct cmdline *l, uint16_t nbPipes);
void jobs();

int executer(char *line)
{
	/* Insert your code to execute the command line
	 * identically to the standard execution scheme:
	 * parsecmd, then fork+execvp, for a single command.
	 * pipe and i/o redirection are not required.
	 */
		struct cmdline *l;
		pid_t  pid;
		//uint16_t i;
		//uint16_t j;

		//printf("Line: %s\n", line);
		// parsecmd free line and set it up to 0 

		l = parsecmd(&line);		
		// If input stream closed, normal termination 
		if (!l) {		  
			terminate(0);
		}
					
		if (l->err) {
			//Syntax error, read another command 
			printf("error: %s\n", l->err);
			return 1;
		}
		
		char **cmd = l->seq[0];
		if (!(strcmp(cmd[0], "jobs"))){
			jobs();
			return 0;
		}

		//execvp(*(*(l->seq)), (*(l->seq)));
		pid = fork();


		//printf("%d\n", pid);
		if (pid==0){
			//done by the children

			if (l->seq[1] == NULL){
					execvp(cmd[0], cmd);
					printf("Command non trouvé %s\n", cmd[0]);
					exit(1);

			} else {
					uint16_t nbPipes=0;

					while (l->seq[nbPipes] != NULL){
						nbPipes++;
					}

					executePipe(l, nbPipes);

		         /* 
		          	printf("seq[%d]: ", i);
		          	for (j=0; cmd[j]!=0; j++) {
		            	printf("'%s' ", cmd[j]);
		            }

					printf("\n");
				*/
			}
			exit(1);

		} else {
			if (!(l->bg)){
				waitpid(pid,NULL,0);
			} else {
				char **cmd2 = l->seq[0];
				insertChild(pid, cmd2[0], liste);
			}
		}
	
	
	//printf("Not implemented: can not execute %s\n", line);

	/* Remove this line when using parsecmd as it will free it */
	//free(line);
	
	return 0;
}

SCM executer_wrapper(SCM x)
{
        return scm_from_int(executer(scm_to_locale_stringn(x, 0)));
}
#endif


void terminate(char *line) {
#ifdef USE_GNU_READLINE
	/* rl_clear_history() does not exist yet in centOS 6 */
	clear_history();
#endif
	if (line)
	  free(line);
	printf("exit\n");
	exit(0);
}


int main() {
        printf("Variante %d: %s\n", VARIANTE, VARIANTE_STRING);
        liste = newList();
#ifdef USE_GUILE
        scm_init_guile();
        /* register "executer" function in scheme */
        scm_c_define_gsubr("executer", 1, 0, 0, executer_wrapper);
#endif

	while (1) {
		//struct cmdline *l;
		char *line=0;
		//int i, j;
		char *prompt = "ensishell>";

		/* Readline use some internal memory structure that
		   can not be cleaned at the end of the program. Thus
		   one memory leak per command seems unavoidable yet */
		line = readline(prompt);
		if (line == 0 || ! strncmp(line,"exit", 4)) {
			terminate(line);
		}

#ifdef USE_GNU_READLINE
		add_history(line);
#endif


#ifdef USE_GUILE
		/* The line is a scheme command */
		if (line[0] == '(') {
			char catchligne[strlen(line) + 256];
			sprintf(catchligne, "(catch #t (lambda () %s) (lambda (key . parameters) (display \"mauvaise expression/bug en scheme\n\")))", line);
			scm_eval_string(scm_from_locale_string(catchligne));
			free(line);
            continue;
            }
#endif

/*
		// parsecmd free line and set it up to 0 
		l = parsecmd( & line);

		// If input stream closed, normal termination 
		if (!l) {		  
			terminate(0);
		}
		
		if (l->err) {
			//Syntax error, read another command 
			printf("error: %s\n", l->err);
			continue;
		}

		if (l->in) printf("in: %s\n", l->in);
		if (l->out) printf("out: %s\n", l->out);
		if (l->bg) printf("background (&)\n");

		// Display each command of the pipe
		for (i=0; l->seq[i]!=0; i++) {
			char **cmd = l->seq[i];
			printf("seq[%d]: ", i);
                        for (j=0; cmd[j]!=0; j++) {
                                printf("'%s' ", cmd[j]);
                        }
			printf("\n");
		}
	*/
		executer(line);
	}

	freeList(liste);
}

void executePipe(struct cmdline *l, uint16_t nbPipes){
	pid_t  pid;

	int fileDescriptors[nbPipes][2];

	// create pipes
	for(int i = 0; i < nbPipes; i++ ) {
    	pipe(fileDescriptors[i]);
	}


	for (int i = 0; i < nbPipes; i++){
		char **cmd = l->seq[i];
		pid = fork();

		if (pid==0){
				//done by the children
				continue;

			} else {
				if (i==0) //premier pere
				{
					dup2(fileDescriptors[i][1], STDOUT_FILENO);
				} else if (i==nbPipes-1){ //derniere pere
					dup2(fileDescriptors[i-1][0], STDIN_FILENO);
				} else {
					dup2(fileDescriptors[i-1][0], STDIN_FILENO);
					dup2(fileDescriptors[i][1], STDOUT_FILENO);
				}
			
			    // closing all pipes
	            for(int j = 0; j < nbPipes; j++ ){
	                close(fileDescriptors[j][0]);
	                close(fileDescriptors[j][1]);
	            }

				execvp(cmd[0], cmd);

				waitpid(pid,NULL,0);
				break;
			}
	}
	exit(1);

}

void deleteTerminated()
{

	int estado;
	for (int i = listLength(liste); i >0 ; i--)
	{
		pid_t PID_i = getPID(i,liste);
		int aux = waitpid(PID_i, &estado, WNOHANG|WUNTRACED);
		if ((PID_i == aux) || (aux < 0))
			deleteChild(PID_i, liste);
	}
}

void jobs(){
	deleteTerminated();
	printList(liste);
}